var glitch = {};

//defaults and constants

glitch.defaults = {

};

glitch.constants = {
  containerStyle: {
    position: "absolute",
    top: "0",
    left: "0",
    margin: "0 auto",
    zIndex: "1"
  },
  insideStyle: {
    position: "absolute",
    backgroundSize: "cover",
    zIndex: "3"
  },
  blends: [
    "multiply",
    "screen",
    "overlay",
    "darken",
    "lighten",
    "color-dodge",
    "color-burn",
    "hard-light",
    "soft-light",
    "difference",
    "exclusion",
    "hue",
    "saturation",
    "color",
    "luminosity"
  ]
};

//helper functions

glitch.applyStyle = (element, style) => {
  Object.keys(style).forEach(key => {
    element.style[key] = style[key];
  });
};

glitch.rect2style = (x, y, width, height, percent) => {
  if(percent){
    return `${x}% ${y}%, ${x+width}% ${y}%, ${x+width}% ${y+height}%, ${x}% ${y+height}%`;
  }
  return `${x}px ${y}px, ${x+width}px ${y}px, ${x+width}px ${y+height}px, ${x}px ${y+height}px`;
};

glitch.generatePercentRect = () => {
  let x = Math.floor(Math.random()*100);
  let y = Math.floor(Math.random()*100);
  return glitch.rect2style(x, y, Math.floor(Math.random()*(150-x)), Math.floor(Math.random()*(150-y)), true);
};

glitch.chaosify = (object, amount=0, interval=200) => {
  let a = Math.floor(Math.random()*20);
  let b = Math.floor(Math.random()*30);
  let t = `translate(${a > 10 ? a+5 : a-25}%, ${b > 15 ? b+15 : b-40}%)`;
  object.style.webkitTransform = t;
  object.style.MozTransform = t;
  object.style.msTransform = t;
  object.style.OTransform = t;
  object.style.transform = t;
  if(amount > 1){
    setTimeout(() => glitch.chaosify(object, amount-1, interval), interval);
  }
}

//Taken from https://stackoverflow.com/a/7419630
glitch.rainbow = (numOfSteps, step) => {
    // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
    // Adam Cole, 2011-Sept-14
    // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
    var r, g, b;
    var h = step / numOfSteps;
    var i = ~~(h * 6);
    var f = h * 6 - i;
    var q = 1 - f;
    switch(i % 6){
        case 0: r = 1; g = f; b = 0; break;
        case 1: r = q; g = 1; b = 0; break;
        case 2: r = 0; g = 1; b = f; break;
        case 3: r = 0; g = q; b = 1; break;
        case 4: r = f; g = 0; b = 1; break;
        case 5: r = 1; g = 0; b = q; break;
    }
    var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
    return (c);
}

//main func

glitch.glitchOut = (object, options={}) => {
  html2canvas(object, {backgroundColor: "transparent"}).then((canvas) => {
    let inner = object.innerHTML;
    object.innerHTML = "";
    let imgsrc = canvas.toDataURL("image/png");
    let container = document.createElement("div");
    let verticalGap = Math.round((options.verticalGap ? options.verticalGap : 0.1) * canvas.height);
    let horizontalGap = Math.round((options.horizontalGap ? options.horizontalGap : 0.1) * canvas.width);
    if(!options.n){options.n = 8}
    if(!options.mode){options.mode = 'shuffle'}
    if(!options.delay){options.delay = 0}
    if(!options.backgroundColor){options.backgroundColor = options.twoColor ? "black" : object.style.backgroundColor}
    glitch.applyStyle(container, glitch.constants.containerStyle);
    container.style.backgroundColor = options.backgroundColor;
    container.style.height = `${canvas.height}px`;
    container.style.width = `${canvas.width}px`;
    container.style.overflow = options.overflow ? options.overflow : "visible";
    switch (options.mode) {
      case 'shuffle':
        for(let i=0;i<options.n;i++) {
          let inside = document.createElement("div");
          glitch.applyStyle(inside, glitch.constants.insideStyle);
          inside.style.background = `url(${imgsrc}) no-repeat 50% 0`;
          inside.style.height = `${canvas.height + 2*verticalGap}px`;
          inside.style.width = `${canvas.width + 2*horizontalGap}px`;
          inside.style.left = `${-1*horizontalGap}px`;
          inside.style.top = `${-1*verticalGap}px`;
          if(options.twoColor) {
            inside.style.backgroundColor = options.foregroundColor ? options.foregroundColor : "white";
          } else {
            if(options.colorList) {
              inside.style.backgroundColor = options.colorList[Math.floor(Math.random() * options.colorList.length)];
            } else {
              inside.style.backgroundColor = glitch.rainbow(i, options.n)
            }
          }
          if(options.blendList) {
            inside.style.backgroundBlendMode = options.blendList[Math.floor(Math.random() * options.blendList.length)];
          } else {
            inside.style.backgroundBlendMode = glitch.constants.blends[Math.floor(Math.random() * glitch.constants.blends.length)];
          }
          if(options.clipPathList) {
            if(options.clipPathList[i]){
              inside.style.clipPath = options.clipPathList[i];
            } else {
              let rects = glitch.generatePercentRect();
              inside.style.webkitClipPath = `polygon(${rects})`;
              inside.style.MozClipPath = `polygon(${rects})`;
              inside.style.msClipPath = `polygon(${rects})`;
              inside.style.OClipPath = `polygon(${rects})`;
              inside.style.clipPath = `polygon(${rects})`;
            }
          } else {
            let rects = glitch.generatePercentRect();
            inside.style.webkitClipPath = `polygon(${rects})`;
            inside.style.MozClipPath = `polygon(${rects})`;
            inside.style.msClipPath = `polygon(${rects})`;
            inside.style.OClipPath = `polygon(${rects})`;
            inside.style.clipPath = `polygon(${rects})`;
          }
          glitch.chaosify(inside, 2, 100);
          container.appendChild(inside);
        }
        //object.innerHTML = "";
        object.appendChild(container);
        if(options.preserveContainer){
          setTimeout(() => {
            container.outerHTML = "";
            object.innerHTML = inner;
          }, options.delay);
          break;
        }
        setTimeout(() => {
          if(options.replacement){
            object.replaceWith(options.replacement);
          } else {
            object.outerHTML = "";
          }
        }, options.delay);
        break;

      case 'break':
        let rects = [
          `polygon(${glitch.rect2style(0, 0, 52, 40, true)})`,
          `polygon(${glitch.rect2style(49, 0, 60, 40, true)})`,
          `polygon(${glitch.rect2style(0, 49, 50, 40, true)})`,
          `polygon(${glitch.rect2style(47, 50, 60, 60, true)})`
        ];
        let tops = [
          '-32%',
          '1%',
          '20%',
          '-15%'
        ];
        let lefts = [
          '-15%',
          '10%',
          '6%',
          '24%'
        ];
        for(let i=0;i<4;i++) {
          let inside = document.createElement("div");
          glitch.applyStyle(inside, glitch.constants.insideStyle);
          inside.style.background = `url(${imgsrc}) no-repeat 50% 0`;
          inside.style.height = `${canvas.height + 2*verticalGap}px`;
          inside.style.width = `${canvas.width + 2*horizontalGap}px`;
          inside.style.left = `${lefts[i]}`;
          inside.style.top = `${tops[i]}`;
          inside.style.webkitClipPath = rects[i];
          inside.style.MozClipPath = rects[i];
          inside.style.msClipPath = rects[i];
          inside.style.OClipPath = rects[i];
          inside.style.clipPath = rects[i];
          container.appendChild(inside);
          //TODO: Add good rect generator for the break mode.
        }
        object.appendChild(container);
        if(options.preserveContainer){
          setTimeout(() => {
            container.outerHTML = "";
            object.innerHTML = inner;
          }, options.delay);
          break;
        }
        setTimeout(() => {
          if(options.replacement){
            object.replaceWith(options.replacement);
          } else {
            object.outerHTML = "";
          }
        }, options.delay);
        break;
    }
  });
}
