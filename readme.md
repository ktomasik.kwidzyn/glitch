<h1>Glitch.js</h1>

A small code snippet

Example usage:
```js
let elem = document.createElement("div");
elem.innerText = "Foo";

glitch.glitchOut(document.getElementById('target'), {
  delay: 800,
  replacement: elem,
  mode: 'shuffle'
);
```

Code above will glitch out element with `target` id and after 800ms, it will replace it with "foo".

TODO:
- [ ] Add proper readme, with all options documented.
- [ ] Expand!
